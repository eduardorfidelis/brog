from django.contrib import admin

# Register your models here.
from blog.models import Post, Categoria, Comentario

admin.site.register(Post)
admin.site.register(Categoria)
admin.site.register(Comentario)
