from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
class Categoria(models.Model):
    nome = models.CharField(max_length = 20)


    def __str__(self):
        return self.nome


class Post(models.Model):
    autor = models.CharField(max_length = 45)
    data_post = models.DateTimeField(auto_now_add = True)
    titulo = models.CharField(max_length = 100)
    descricao = models.TextField()
    texto = RichTextField()
    banner = models.ImageField(null=True, upload_to='posts')
    categoria = models.ForeignKey(Categoria, on_delete=models.SET_NULL, blank=True, null=True, related_name='posts')

    def __str__(self):
        return self.titulo

class Comentario(models.Model):
    texto = models.TextField()
    data_post = models.DateTimeField(auto_now_add = True)
    autor = models.CharField(max_length = 45, blank=True, null=True,)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comentarios',)
    autor = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='comentarios',
    )
    def __str__(self):
        return self.texto