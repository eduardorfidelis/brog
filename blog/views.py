from django.db.models import Q
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView
from django.shortcuts import redirect
from blog.models import Post, Categoria, Comentario

# Create your views here.
from blog.models import Post, Categoria, Comentario

class LayoutView:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categorias'] = Categoria.objects.all() 
        return context

class PostListView(LayoutView, ListView):
    model = Post


class CategoriaDetailView(LayoutView, DetailView):
    model = Categoria


class PostDetailView(LayoutView, DetailView):
    model = Post

class ComentarioDetailView(LayoutView, DetailView):
    model = Comentario

class SearchView(LayoutView, ListView):
    model = Post
    template_name = 'search'

    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = Post.objects.filter(
            Q(titulo__icontains=query) | 
            Q(texto__icontains=query) 
            )
        return object_list

class ComentarioCreateView(LayoutView, CreateView):
    model = Comentario
    fields = ['texto']

    def form_valid(self, form):
        post_id = self.kwargs['post']
        self.object = form.save(commit=False)
        self.object.autor = self.request.user
        self.object.post = Post.objects.get(id=post_id)
        self.object.save()
        return redirect('post-detail', pk=post_id)
