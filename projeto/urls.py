from django.contrib import admin
from django.urls import path
from blog import views as blog_views
from django.conf import settings
from django.conf.urls.static import static
from blog.models import Post, Comentario
from django.urls import path, include

urlpatterns = [
    path('', blog_views.PostListView.as_view(), name = 'post_list'),
    path('', blog_views.PostListView.as_view(), name='post-list'),
    path('comentario/<int:post>/', blog_views.ComentarioCreateView.as_view(), name='comentario'),
    path('pesquisa/', blog_views.SearchView.as_view(), name='search'),
    path('categoria/<int:pk>/', blog_views.CategoriaDetailView.as_view(), name='categoria-detail'),
    path('post/<int:pk>/', blog_views.PostDetailView.as_view(), name='post-detail'),
    path('admin/', admin.site.urls),
    path('usuarios/', include('django.contrib.auth.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)